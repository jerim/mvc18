<?php

namespace App\Controllers;

use \app\models\User;
require_once '../app/models/User.php';


class UserController
{

    function __construct()
    {
        // echo "En UserController ";
    }

     public function index(){
         $users= User::paginate(2);
         $rowCount=User::rowCount();
     //  $users= User::all(); //acceder a metodos estaticos sin obj




          if(isset($_REQUEST['page'])){
            $page = (integer) $_REQUEST['page'];

         }else{
                $page=1;
         }

         $page =ceil($rowCount /2);

     require "../app/views/user/index.php";

    }

    public function show($arguments){
        $id = (int)$arguments[0];
        $user=  User::find($id);//buscar un obj de user, el id
        require "../app/views/user/show.php";

    }

    public function create(){

     require "../app/views/user/create.php";
    }

    public function store(){
        $user= new User();
        $user->name= $_REQUEST['name'];
        $user->surname= $_REQUEST['surname'];
        $user->age= $_REQUEST['age'];
        $user->email= $_REQUEST['email'];

        $user->insert();

    header('Location: /user');

    }

   public function delete($arguments){
    $id = (int)$arguments[0];
    User::delete($id);//buscar un obj de user, el id
    header('Location: /user');
   }

   public function edit($arguments){
    $id = (int)$arguments[0];
    $user=User::find($id);//buscar un obj de user, el id

    require "../app/views/user/edit.php";
   }

   public function update(){
        $user = new User();
        $user->id = $_REQUEST['id'];
        $user->name = $_REQUEST['name'];
        $user->surname = $_REQUEST['surname'];
        $user->age = $_REQUEST['age'];
        $user->email = $_REQUEST['email'];
        $user->save();
        header('Location: /user');
   }

}


  ?>
