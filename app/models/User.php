<?php
namespace App\models;

use PDO;
use Core\Model;

require_once '../core/Model.php';

class User extends Model
{

    function __construct()
    {

    }

public static function all(){
$db = User::db();
$statement = $db->query('SELECT * FROM users');
$users = $statement->fetchAll(PDO::FETCH_CLASS,User::class);
return $users;

}

public function paginate($size =10){
    if(isset($_REQUEST['page'])){
    $page = (integer) $_REQUEST['page'];

    }else{
        $page=1;
    }

    $offset =($page -1) * $size;

$db = User::db();
$statement = $db->prepare('SELECT * FROM users LIMIT :pagesize OFFSET :offset');
$statement->bindValue('pagesize',$size, PDO::PARAM_INT);
$statement->bindValue('offset',$offset, PDO::PARAM_INT);
$statement->execute();

$users = $statement->fetchAll(PDO::FETCH_CLASS,User::class);
return $users;

}

public static function rowCount(){
$db = User::db();
$statement = $db->prepare('SELECT count(id) as count FROM users');
$statement->execute();

$rowCount = $statement->fetch(PDO::FETCH_ASSOC);
return $rowCount['count'];

}


public static function find($id){
$db = User::db();
$statemet = $db->prepare('SELECT * FROM users WHERE id=?');
$statemet->bindValue(1, $id, PDO::PARAM_INT);
$statemet->execute();
$statemet-> setFetchMode(PDO::FETCH_CLASS,User::class);
$users = $statemet->fetchAll(PDO::FETCH_CLASS);
return $users[0];
}

public function insert(){

$db = User::db();
$statemet = $db->prepare('INSERT INTO users(name,surname,age,email) VALUES (:name ,:surname, :age, :email )');

$statemet->bindValue(':name', $this->name);
$statemet->bindValue(':surname', $this->surname);
$statemet->bindValue(':age', $this->age);
$statemet->bindValue(':email', $this->email);

return $statemet->execute(); //devuelve el num de filas

}

public function delete($id){

$db = User::db();
$statemet = $db->prepare('DELETE FROM users WHERE id= :id');
$statemet->bindValue(':id',$id);
return $statemet->execute(); //devuelve el num de filas
}

public function save(){

$db = User::db();
$statemet = $db->prepare('UPDATE users SET name=:name, surname=:surname,age=:age,email=:email WHERE id=:id ;');

$statemet->bindValue(':id', $this->id);
$statemet->bindValue(':name', $this->name);
$statemet->bindValue(':surname', $this->surname);
$statemet->bindValue(':age', $this->age);
$statemet->bindValue(':email', $this->email);
return $statemet->execute(); //devuelve el num de filas
}

}


?>
