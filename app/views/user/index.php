<!doctype html>
<html lang="en">
  <head>
   <?php require "../app/views/parts/head.php" ?>
  </head>
  <body>

<?php require "../app/views/parts/header.php" ?>

    <main role="main" class="container">
     <br><br><br><br>
      <div class="starter-template">
        <h1>User </h1>
        <p class="lead">Pagina de presentación</p>

        <table class="table table-striped">
          <thead>
            <tr>
            <th>Id</th>
            <th>Nombre</th>
            <th>Apellido</th>
            <th>Edad</th>
            <th>Email</th>
          </tr>
          </thead>
          <tbody>
          <?php foreach ($users as $user): ?>
            <tr>
              <td><?php echo $user->id ?></td>
              <td><?php echo $user->name ?></td>
              <td><?php echo $user->surname ?></td>
              <td><?php echo $user->age ?></td>
              <td><?php echo $user->email ?></td>
              <td><a href="/user/show/<?php echo $user->id ?>">Ver</a></td>
              <td><a href="/user/delete/<?php echo $user->id ?>">borrar</a></td>
              <td><a href="/user/edit/<?php echo $user->id ?>">actualizar</a></td>
            </tr>
          <?php endforeach ?>
          </tbody>
        </table>
          <br>
            <?php for ($i=1;$i<=$page; $i++) :?>
                 <a href="/user?page=<?php echo $i ?>" class="btn btn-primary"> <?php echo $i ?></a>
             <?php endfor?>
          <br>
          <a href="/user/create">Nuevo usuario</a>
      </div>

    </main><!-- /.container -->
<?php require "../app/views/parts/footer.php" ?>
 </body>
 <?php require "../app/views/parts/scripts.php" ?>
</html>
