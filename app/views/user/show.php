<!doctype html>
<html lang="es">
  <head>
   <?php require "../app/views/parts/head.php" ?>
  </head>
  <body>

<?php require "../app/views/parts/header.php" ?>

    <main role="main" class="container">
     <br><br><br><br>
      <div class="starter-template">
        <h1>Detalles de Usuario</h1>

        <table class="table table-striped">
          <thead>
            <tr>
            <th>Id</th>
            <th>Nombre</th>
            <th>Apellido</th>
            <th>Edad</th>
            <th>Email</th>
          </tr>
          </thead>
          <tbody>

            <tr>
              <td><?php echo $user->id ?></td>
              <td><?php echo $user->name ?></td>
              <td><?php echo $user->surname ?></td>
              <td><?php echo $user->age ?></td>
              <td><?php echo $user->email ?></td>
            </tr>

          </tbody>
        </table>
      </div>
      <a href="/user">Volver a usuario</a>
    </main><!-- /.container -->
<?php require "../app/views/parts/footer.php" ?>
 </body>
  <?php require "../app/views/parts/scripts.php" ?>
</html>
