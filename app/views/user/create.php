<!doctype html>
<html lang="es">
  <head>
   <?php require "../app/views/parts/head.php" ?>
  </head>
  <body>

<?php require "../app/views/parts/header.php" ?>

    <main role="main" class="container">
     <br><br><br><br>
      <div class="starter-template">
        <h1>Alta de Usuario</h1>
        <form method="post" action="/user/store">
            <div class="form-group">
             <label for="text">Nombre :</label>
             <input type="text" name="name" class="form-control" >
             </div>

             <div class="form-group">
            <label for="text">Apellido :</label>
            <input type="text" name="surname" class="form-control" >
            </div>

            <div class="form-group">
             <label for="age">edad :</label>
             <input type="age" name="age" class="form-control" >
            </div>

            <div class="form-group">
            <label for="email">Email :</label>
             <input type="email" name="email" class="form-control" >
            </div>
            <button type="submit" class="btn btn-default">Enviar</button>
  </div>
        </form>
          <a href="/user">Volver a usuario</a>
      </div>
    </main><!-- /.container -->
        <?php require "../app/views/parts/footer.php" ?>
</body>
        <?php require "../app/views/parts/scripts.php" ?>
</html>
