<?php

namespace Core;




class App
{

    function __construct()
    {
        //echo "Clase App <br>";

     if(isset($_GET['url'])){

       $url= $_GET['url'];

     }else{
          $url='home';
       }

     $arguments = explode('/',trim($url,'/'));
     $controllerName = array_shift($arguments);
     $controllerName = ucwords($controllerName) . "Controller";
     if(count($arguments)){
         $method= array_shift($arguments);
     }else{
        $method = "index";
     }



     $file = "../app/controllers/$controllerName" . ".php";
     if (file_exists($file)){
        require_once $file; //requiere una vez
     }else{
        header("HTTP/1.0 404 Not Found");
     //   echo "no encontrado";
        die(); // se muere
     }

     $controllerName='\\App\\Controllers\\' . $controllerName;
     $controllerObject = new $controllerName;

     if(method_exists($controllerName,$method)){
      try {
         $controllerObject -> $method($arguments);

      } catch (\Exception $e) {
            header("HTTP/1.0 500 Internal Error");
              echo $e->getMessage();
              echo "<pre>";
              echo $e->getTraceAsString();
      }


     }else{
        header("HTTP/1.0 404 Not Found");
      //  echo "no encontrado";
        die(); // se muere
     }




    }
}
